# GNOMEBOT
SimulationCraft Bot for discord.

Based on original work from https://github.com/stokbaek/simc-discord
modified to output more information on Discord and with added interactivity

The following things are needed to run the bot:
* Python 3.5+
* Python Discord lib: https://github.com/Rapptz/discord.py
* Webservice on the server to hand out a link to the finished simulation.
* Simulationcraft TCI 7.2 compatible
* Blizzard API key (This is needed to use armory): Keys needs to be added in user_data and it can be generated here: https://dev.battle.net

Tested systems:
- [x] Ubuntu Zesti
- [x] Debian 8.7 (with python 3.5)
- Or use docker : see gnomebot-docker project

The output from simulationcraft can be found: `<WEBSITE>/debug/simc.sterr or simc.stout`. These files are live updated during a simulation.

***Help for simulation through Discord:***

*Basic Options:*
```
-c  -character    in-game name
-r  -realm        realm name (Default is Magtheridon)
-s  -scale        yes/no (Default is no)
-t  -data         replace talent format is xxxxxxx with x [1-3] 
-f  -fightstyle   Choose between different fightstyles : light,heavy,patchwerk,... see helpfiles
-v  -version      Gives the version of simulationcraft being used
```
* Simulate using armory with stat scaling:

`!sim -character NAME -scale yes`
* Simulate using other talents without stat scaling:

`!sim -character NAME -t 1213111`

